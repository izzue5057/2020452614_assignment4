function validateForm()
{

	var question1 = document.forms["form"]["question1"].value;
	var question2 = document.forms["form"]["question2"].value;
	var question3 = document.forms["form"]["question3"].value;
	var question4 = document.forms["form"]["question4"].value;
	var question5 = document.forms["form"]["question5"].value;
	var question6 = document.forms["form"]["question6"].value;
	var question7 = document.forms["form"]["question7"].value;
	var question8 = document.forms["form"]["question8"].value;
	var question9 = document.forms["form"]["question9"].value;
	var question10 = document.forms["form"]["question10"].value;
	var name = document.forms["form"]["name"].value;
	
	if(question1==""){
		alert("Oops!! Question 1 is required");
		return false;
	}
	if(question2==""){
		alert("Oops!! Question 2 is required");
		return false;
	}
	if(question3==""){
		alert("Oops!! Question 3 is required");
		return false;
	}
	if(question4==""){
		alert("Oops!! Question 4 is required");
		return false;
	}
	if(question5==""){
		alert("Oops!! Question 5 is required");
		return false;
	}
	if(question6==""){
		alert("Oops!! Question 6 is required");
		return false;
	}
	if(question7==""){
		alert("Oops!! Question 7 is required");
		return false;
	}
	if(question8==""){
		alert("Oops!! Question 8 is required");
		return false;
	}
	if(question9==""){
		alert("Oops!! Question 9 is required");
		return false;
	}
	if(question10==""){
		alert("Oops!! Question 10 is required");
		return false;
	}
	if(name==""){
		alert("Oops!! Your name is required");
		return false;
	}

	var score = getScore();

	if(score<=4)
	{
		alert("Keep trying, "+name+ "! You answered "+ score+ " out of 10 correctly");
	}
	else if(score>=5 & score<=9)
	{
		alert("Way to go, " +name+ "You got " + score + " out of 10 correctly");
	}
	else
	{
		alert("Congratulation " +name+ "! You got " + score + " out of 10");
	}
}

function getScore(){
		var question1 = document.forms["form"]["question1"].value;
		var question2 = document.forms["form"]["question2"].value;
		var question3 = document.forms["form"]["question3"].value;
		var question4 = document.forms["form"]["question4"].value;
		var question5 = document.forms["form"]["question5"].value;
		var question6 = document.forms["form"]["question6"].value;
		var question7 = document.forms["form"]["question7"].value;
		var question8 = document.forms["form"]["question8"].value;
		var question9 = document.forms["form"]["question9"].value;
		var question10 = document.forms["form"]["question10"].value;
		var score =0;

						
		if(document.getElementById("question1").value="Cascading Style Sheets")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}
	
		if(document.getElementById("question2").value=="style")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}
	
		if(document.getElementById("question3").value=="background-color")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}

		if(document.getElementById("question4").value=="font-color")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}

		if(document.getElementById("question5").value=="font-size")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}

		if(document.getElementById("question6").value=="weight:bold;")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}

		if(document.getElementById("question7").value=="Separate each selector with a comma")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}
	
		if(document.getElementById("question8").value=="static")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}
	
		if(document.getElementById("question9").value=="#demo")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}
	
		if(document.getElementById("question10").value=="No")
		{
			score = score+1;
		}
		else
		{
			score = score;
		}

		result = score;

		return result;
}
